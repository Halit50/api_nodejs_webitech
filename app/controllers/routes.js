const Show = require('./users/show.js');
const Post = require('./users/post.js');
const Put = require('./users/put.js');
const Delete = require('./users/delete.js');

module.exports = {
    users: {
        Show
    },
    user: {
        Post,
        Delete,
        Put
    }
}